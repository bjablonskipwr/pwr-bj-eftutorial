﻿using System;

namespace EF_Tutorial
{
    public class Dziekanat
    {

        /// <summary>
        /// Prosta metoda, ktora wypisuje wszystkie grupy i studentow z bazy danych.
        /// Normalnie takie metody powinny się znaleźć w odpowiednich klasach do obsługi danych
        /// </summary>
        public void WypiszStudentow()
        {
            // Do odwoływania się do bazy danych używamy obiektu kontekstu
            using (var ctx = new StudiaDbContext())
            {
                // EF domyślnie wykorzystuje lazy loading - jeżeli coś nie jest potrzebne, to nie jest ładowane
                // Aby EF automatycznie moglo zaladowac relacje, muszą one byc zdefiniowane jako virtual
                // W przeciwnym wypadku musielibysmy jawnie zadeklarowac powiazanie przez dolaczenie do zapytania tablicy Students, np.: 
                // var grupy = ctx.Grupa.Include("Students");

                foreach (var g in ctx.Grupa)
                {
                    Console.WriteLine("----------------");
                    Console.WriteLine(g);
                    foreach (var s in g.Students)
                    {
                        Console.WriteLine(s);
                    }
                }
            }
        }

        public void DodajPrzykladowoStudenta()
        {
            using (var ctx = new StudiaDbContext())
            {
                // Tworzymy grupę w pamięci
                Grupa grupa = new Grupa() { Name = "Aniołki Charliego" };

                Student student;

                // Dodajemy studenta do grupy
                student = new Student() { Name = "Hania", Birthdate = new DateTime(1990, 10, 10), AdresZamieszkania = "Kraków" };
                grupa.Students.Add(student);

                // Dodajemy kolejnego studenta do grupy
                student = new StudentZaoczny() { Name = "Krysia", Birthdate = new DateTime(1980, 10, 10), AdresZamieszkania = "Suwałki" };
                grupa.Students.Add(student);

                // Grupę dodajemy do kolekcji reprezentującej dane w bazie danych
                ctx.Grupa.Add(grupa);

                // Zapisujemy zmienne przechowywane w kontekście
                ctx.SaveChanges();
            }
        }

    }
}
