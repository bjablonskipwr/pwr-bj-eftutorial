﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace EF_Tutorial
{

    public class Student
    {
        // EF w ramach konwencji zaklada, ze klucz glowny ma nazwe Id.
        public int Id { get; set; }
        public string Name { get; set; }
        public string AdresZamieszkania { get; set; }
        public DateTime Birthdate { get; set; }

        // Zmienna GrupaId jest nullable, to znaczy, że moze byc ustawiona jako null.
        // Dzieki temu nie kazdy student musi byc przypisany do grupy.
        // EF w ramach konwencji zaklada, ze klucz obcy do tabeli zewnetrznej to nazwa tabli zakonczona Id.
        public int? GrupaId { get; set; }

        // Jezeli chcemy, aby klucz obcy mial inna nazwe niz konwencja to mozemy to zrobic przez atrybut ForeighKey.
        [ForeignKey("GrupaId")]
        // Referencja do zmiennej Grupa jest virtual. Dzieki temu EF bedzie mogl wykorzystywac lazy loading
        // do automatycznego zaladowania powiazanych danych.
        public virtual Grupa Grupa { get; set; }

        public override string ToString()
        {
            return string.Format("St Id={0}, Name={1}, Adres={2}, Ur.={3}, Grupa={4}", Id, Name, AdresZamieszkania, Birthdate, Grupa == null ? "null" : Grupa.Name);
        }
    }

    /// <summary>
    /// Takie oznaczenie mówi - do dziedziczenia używaj osobnych tabel. Podejscie TPT - Table Per Type.
    /// Domyslnie EF przyjmuje podejscie TPH - Table Per Hierarchy.
    /// Szczegóły - patrz http://weblogs.asp.net/manavi/inheritance-mapping-strategies-with-entity-framework-code-first-ctp5-part-1-table-per-hierarchy-tph 
    /// </summary>
    [Table("Zaoczni")]
    public class StudentZaoczny : Student
    {
        public decimal StanKonta { get; set; }

        public override string ToString()
        {
            return string.Format("{0}, StanKonta={1}", base.ToString(), StanKonta);
        }

    }
}
