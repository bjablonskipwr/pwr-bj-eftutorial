﻿using System.Data.Entity;

namespace EF_Tutorial
{
    public class StudiaDbContext : DbContext
    {
        // Aby podac wlasna nazwe bazy danych, nalezy wywolac konstruktor bazowy z nazwą jako parametrem.
        public StudiaDbContext()
            : base("StudiaNaPWr")
        {
            // Użyj klasy StudiaDbInitializer do zainicjalizowania bazy danych.
            Database.SetInitializer(new StudiaDbInitializer());
        }

        public DbSet<Student> Student { get; set; }

        public DbSet<Grupa> Grupa { get; set; }
    }

}
