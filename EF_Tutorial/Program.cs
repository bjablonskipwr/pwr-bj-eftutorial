﻿using System;

namespace EF_Tutorial
{
    class Program
    {

        static void Main(string[] args)
        {
            var dziekanat = new Dziekanat();

            // Nie za każdym uruchomieniem chcemy dodawać tych samych studentów
            // Dlatego ta przykładowa metoda jest wykomentowana. 
            
            // dziekanat.DodajPrzykladowoStudenta();

            // Aktualnie początkowe dane dla przykładu ładowane są przez klasę StudiaDbInitializer.

            dziekanat.WypiszStudentow();

            Console.WriteLine();
            Console.WriteLine("Happy end!");
            Console.ReadKey();
        }
    }
}
