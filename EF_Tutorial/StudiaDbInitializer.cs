﻿using System;

namespace EF_Tutorial
{
    /// <summary>
    /// Specjalna klasa, któa ma pomagać w inicjalizowaniu bazy danych.
    /// Może dziedziczyć po kilku klasach - jeżeli dziedziczy po DropCreateDatabaseIfModelChanges, 
    /// to baza zostanie przebudowana przy każdej zmianie modelu.
    /// Jezeli dziedziczy po DropCreateDatabaseAlways to przy każdym uruchomieniu baza danych tworzona jest od nowa.
    /// </summary>
    public class StudiaDbInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<StudiaDbContext>
    {

        /// <summary>
        /// Specjalna metoda, która jest wywoływana raz po przebudowaniu bazy danych.
        /// Założenie jest, że baza jest pusta, więc trzeba ją wypełnić początkowymi danymi.
        /// </summary>
        /// <param name="context"></param>
        protected override void Seed(StudiaDbContext context)
        {
            Grupa grupa;

            grupa = new Grupa() { Name = "ART" };
            grupa.Students.Add(new Student() { Name = "Janek", AdresZamieszkania = "Wrocław", Birthdate = new DateTime(2000, 1, 1) });
            grupa.Students.Add(new Student() { Name = "Ola", AdresZamieszkania = "Gliwice", Birthdate = new DateTime(2001, 2, 2) });
            context.Grupa.Add(grupa);

            grupa = new Grupa() { Name = "ASI" };
            grupa.Students.Add(new Student() { Name = "Zenek", AdresZamieszkania = "Poznań", Birthdate = new DateTime(2002, 1, 1) });
            grupa.Students.Add(new Student() { Name = "Ala", AdresZamieszkania = "Warszawa", Birthdate = new DateTime(2003, 2, 2) });
            context.Grupa.Add(grupa);

            grupa = new Grupa() { Name = "ART_Zaoczne" };
            grupa.Students.Add(new StudentZaoczny() { Name = "Franek", AdresZamieszkania = "Łódź", Birthdate = new DateTime(2004, 1, 1), StanKonta = 120 });
            grupa.Students.Add(new StudentZaoczny() { Name = "Kasia", AdresZamieszkania = "Rzeszów", Birthdate = new DateTime(2005, 2, 2), StanKonta = 240 });
            context.Grupa.Add(grupa);

            context.SaveChanges();
            base.Seed(context);
        }
    }
}
