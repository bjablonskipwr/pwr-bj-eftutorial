﻿using System.Collections.Generic;

namespace EF_Tutorial
{
    public class Grupa
    {
        public int Id { get; set; }
        public string Name { get; set; }

        // Referencja do Grupy jest virtual. Dzieki temu EF bedzie mogl wykorzystywac lazy loading
        // do automatycznego zaladowania powiazanych danych.
        public virtual List<Student> Students { get; set; } = new List<Student>();

        public override string ToString()
        {
            return string.Format("Grupa Id={0}, Name={1}, Licznosc={2}", Id, Name, Students == null ? 0 : Students.Count);
        }

    }
}
