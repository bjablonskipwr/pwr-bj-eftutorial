Materiały do zajęć - **Platformy programistyczne .NET i Java**

W ramach fazy 3.0 aplikacji JTTT podpinamy bazę danych z wykorzystaniem Entity Framework i podejścia Code First.

W tym repozytorium znajduje się trochę uporządkowana testowa aplikacja, którą tworzymy na zajęciach wprowadzających do EF. Mini-projekt pt. "Uśmiechnięty Dziekanat" ;)

Uwaga - przed uruchomieniem sprawdzić app.config i ustawić odpowiednie podłączenie do bazy danych. Podane są konfiguracje przy zainstalowanym SQL Express i bez niego.

